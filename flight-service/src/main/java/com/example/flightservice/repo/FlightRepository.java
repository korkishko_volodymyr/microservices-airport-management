package com.example.flightservice.repo;

import com.example.flightservice.model.Flight;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends CrudRepository<Flight,Integer> {
    List<Flight> findByPlaneId(Integer planeId);
}
