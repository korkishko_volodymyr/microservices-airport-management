package com.example.flightservice.client;

import com.example.ticketservice.model.Ticket;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "ticket-service")
public interface TicketClient {
    @GetMapping("/flight/{flightId}")
    List<Ticket> getTicketsByFlight(@PathVariable("flightId") Integer flightId);
}
