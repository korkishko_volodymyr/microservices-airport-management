package com.example.flightservice.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String date;
    private int duration;
    private int planeId;
}