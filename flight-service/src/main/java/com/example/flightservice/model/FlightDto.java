package com.example.flightservice.model;

import com.example.ticketservice.model.Ticket;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FlightDto {
    private int id;
    private List<Ticket> tickets;
}
