package com.example.flightservice.controller;

import com.example.flightservice.client.TicketClient;
import com.example.flightservice.model.Flight;
import com.example.flightservice.model.FlightDto;
import com.example.flightservice.service.FlightService;
import com.example.ticketservice.model.Ticket;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FlightController {

    private final FlightService flightService;
    private final TicketClient ticketClient;

    public FlightController(FlightService flightService, TicketClient ticketClient) {
        this.flightService = flightService;
        this.ticketClient = ticketClient;
    }

    @GetMapping
    public List<Flight> getAll() { return flightService.getAll(); }

    @GetMapping("/{flightId}")
    public Flight get(@PathVariable("flightId") Integer flightId) {
        return flightService.findById(flightId);
    }

    @GetMapping("/{flightId}/with-tickets")
    public FlightDto getTicketsByFlight(@PathVariable("flightId") Integer flightId) {
        final List<Ticket> tickets = ticketClient.getTicketsByFlight(flightId);
        return new FlightDto(flightId,tickets);
    }

    @GetMapping("/plane/{planeId}")
    public List<Flight> getFlightsByPlane(@PathVariable("planeId") Integer planeId){
        return flightService.findByPlaneId(planeId);
    }

    @PostMapping
    public void save(@RequestBody Flight flight) { flightService.save(flight); }

    @DeleteMapping("/{flightId}")
    public void delete(@PathVariable("flightId") Integer flightId) { flightService.delete(flightId); }
}
