package com.example.flightservice.service;

import com.example.flightservice.model.Flight;

import java.util.List;

public interface FlightService {

    List<Flight> findByPlaneId(Integer planeId);

    List<Flight> getAll();

    Flight findById(Integer id);

    void save(final Flight airline);

    void delete(Integer id);

}
