package com.example.flightservice.service;

import com.example.flightservice.model.Flight;
import com.example.flightservice.repo.FlightRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;

    public FlightServiceImpl(FlightRepository flightRepository) { this.flightRepository = flightRepository; }

    public void save(final Flight airline) { flightRepository.save(airline); }

    public List<Flight> findByPlaneId(Integer planeId) { return flightRepository.findByPlaneId(planeId); }

    public List<Flight> getAll() { return (List<Flight>) flightRepository.findAll(); }

    public Flight findById(Integer id) { return flightRepository.findById(id).orElse(new Flight()); }

    public void delete(Integer id){ flightRepository.deleteById(id); }
}
