package com.example.airportservice.client;

import com.example.airlineservice.model.Airline;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "airline-service")
public interface AirlineClient {
    @GetMapping("/airport/{airportId}")
    List<Airline> getAirlinesByAirport(@PathVariable("airportId") Integer airportId);
}