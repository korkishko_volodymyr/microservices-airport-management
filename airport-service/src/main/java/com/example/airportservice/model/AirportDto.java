package com.example.airportservice.model;

import com.example.airlineservice.model.Airline;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AirportDto {
    private int id;
    private List<Airline> airlines;
}
