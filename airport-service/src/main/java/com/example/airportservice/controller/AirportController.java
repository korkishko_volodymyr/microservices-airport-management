package com.example.airportservice.controller;

import com.example.airlineservice.model.Airline;
import com.example.airportservice.client.AirlineClient;
import com.example.airportservice.model.Airport;
import com.example.airportservice.model.AirportDto;
import com.example.airportservice.service.AirportService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AirportController {

    private final AirportService airportService;
    private final AirlineClient airlineClient;

    public AirportController(AirportService airportService, AirlineClient airlineClient) {
        this.airportService = airportService;
        this.airlineClient = airlineClient;
    }

    @GetMapping
    public List<Airport> getAll(){ return airportService.getAll(); }

    @GetMapping("/{airportId}")
    public Airport get(@PathVariable("airportId") Integer airportId){ return airportService.findById(airportId); }

    @GetMapping("/{airportId}/with-airlines")
    public AirportDto getAirportWithAirlines(@PathVariable("airportId") Integer airportId){
        final List<Airline> airlines = airlineClient.getAirlinesByAirport(airportId);
        return new AirportDto(airportId,airlines);
    }

    @PostMapping
    public void save(@RequestBody Airport airport){ airportService.save(airport); }


    @DeleteMapping("/{airportId}")
    public void delete(@PathVariable("airportId") Integer airportId){ airportService.delete(airportId); }

}
