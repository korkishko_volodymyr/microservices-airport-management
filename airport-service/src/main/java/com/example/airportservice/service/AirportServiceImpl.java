package com.example.airportservice.service;

import com.example.airportservice.model.Airport;
import com.example.airportservice.repo.AirportRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    private final AirportRepository airportRepository;

    public AirportServiceImpl(AirportRepository airportRepository) { this.airportRepository = airportRepository; }

    public void save(final Airport airport) { airportRepository.save(airport); }

    public List<Airport> getAll() { return (List<Airport>) airportRepository.findAll(); }

    public Airport findById(Integer id) { return airportRepository.findById(id).orElse(new Airport()); }

    public void delete(Integer id){ airportRepository.deleteById(id); }
}
