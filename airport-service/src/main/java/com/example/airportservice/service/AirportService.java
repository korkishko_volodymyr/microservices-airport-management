package com.example.airportservice.service;

import com.example.airportservice.model.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> getAll();

    Airport findById(Integer id);

    void save(final Airport airline);

    void delete(Integer id);
}
