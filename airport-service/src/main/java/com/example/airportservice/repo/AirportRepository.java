package com.example.airportservice.repo;

import com.example.airportservice.model.Airport;
import org.springframework.data.repository.CrudRepository;

public interface AirportRepository extends CrudRepository<Airport, Integer> { }
