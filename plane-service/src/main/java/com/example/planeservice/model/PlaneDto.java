package com.example.planeservice.model;

import com.example.flightservice.model.Flight;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PlaneDto {
    private int id;
    private List<Flight> flights;
}
