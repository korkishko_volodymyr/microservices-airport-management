package com.example.planeservice.client;

import com.example.flightservice.model.Flight;
import com.example.planeservice.model.Plane;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "flight-service")
public interface FlightClient {
    @GetMapping("/plane/{planeId}")
    List<Flight> getFlightsByPlane(@PathVariable("planeId") Integer planeId);
}