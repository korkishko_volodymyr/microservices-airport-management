package com.example.planeservice.repo;

import com.example.planeservice.model.Plane;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaneRepository extends CrudRepository<Plane, Integer> {
    List<Plane> findByAirlineId(Integer airlineId);
}
