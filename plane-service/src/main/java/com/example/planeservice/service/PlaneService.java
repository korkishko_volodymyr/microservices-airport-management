package com.example.planeservice.service;

import com.example.planeservice.model.Plane;

import java.util.List;

public interface PlaneService {

    List<Plane> findByAirlineId(Integer airlineId);

    List<Plane> getAll();

    Plane findById(Integer id);

    void save(final Plane airline);

    void delete(Integer id);

}
