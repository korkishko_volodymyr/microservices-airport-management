package com.example.planeservice.service;

import com.example.planeservice.model.Plane;
import com.example.planeservice.repo.PlaneRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaneServiceImpl implements PlaneService {

    private final PlaneRepository planeRepository;

    public PlaneServiceImpl(PlaneRepository planeRepository) { this.planeRepository = planeRepository; }

    public void save(final Plane plane) { planeRepository.save(plane); }

    public List<Plane> findByAirlineId(Integer airlineId) { return planeRepository.findByAirlineId(airlineId); }

    public List<Plane> getAll() { return (List<Plane>) planeRepository.findAll(); }

    public void delete(Integer id) { planeRepository.deleteById(id); }

    public Plane findById(Integer id){ return planeRepository.findById(id).orElse(new Plane()); }
}
