package com.example.planeservice.controller;

import com.example.flightservice.model.Flight;
import com.example.planeservice.client.FlightClient;
import com.example.planeservice.model.Plane;
import com.example.planeservice.model.PlaneDto;
import com.example.planeservice.service.PlaneService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlaneController {

    private final PlaneService planeService;
    private final FlightClient flightClient;

    public PlaneController(PlaneService planeService, FlightClient flightClient) {
        this.planeService = planeService;
        this.flightClient = flightClient;
    }

    @GetMapping
    public List<Plane> getAll(){ return planeService.getAll(); }

    @GetMapping("/{planeId}")
    public Plane get(@PathVariable("planeId") Integer planeId){ return planeService.findById(planeId); }

    @GetMapping("/{planeId}/with-flights")
    public PlaneDto getFlightsByPlane(@PathVariable("planeId") Integer planeId){
        final List<Flight> flights = flightClient.getFlightsByPlane(planeId);
        return new PlaneDto(planeId,flights);
    }

    @GetMapping("/airline/{airlineId}")
    public List<Plane> getPlanesByAirline(@PathVariable("airlineId") Integer airlineId){
        return planeService.findByAirlineId(airlineId);
    }

    @PostMapping
    public void save(@RequestBody Plane plane){ planeService.save(plane); }

    @DeleteMapping("/{planeId}")
    public void delete(@PathVariable("planeId") Integer planeId) { planeService.delete(planeId); }
}
