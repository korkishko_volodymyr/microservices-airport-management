# Eureka Microservice

## Description
See https://spectrum.aerlingus.com/confluence/display/esb/Eureka.

## Building the Microservice
Run the following maven command
- `mvn clean install`


## Launching the Microservice
You can launch the Microservice using
```
java 
-Dlogging.root=${logging.root}
-Deureka.client.serviceUrl.defaultZone=${eureka.serviceUrl}
-Dspring.profiles.active=${env}
-jar eureka-${version}.jar
```

- `${logging.root}` is the root location of the log files
- `${eureka.serviceUrl}` is the Eureka service URL(s)
- `${env}` is the environment, `dev`, `sit`, `uat`, `ppd`, `prod`
- `${version}` is the version of the artifact


Note that supplying `-Deureka.client.serviceUrl.defaultZone` is optional. If its not supplied, the zones will be taken from the `dev`, `uat`, `sit`, `ppd` and `prod` profiles.

### Example
```
java
-Dlogging.root=file:///${user.home}/applog
-Deureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka/
-Dspring.profiles.active=dev,dev01
-jar eureka-1.0.0-GDM-SNAPSHOT.jar
```

## Validate Application Deployment
Search for the string `Tomcat started on port(s):` in the log file `${logging.root}/eureka/eureka.log`

## Enable Peer Awareness
Eureka can be made more resilient and available by running [multiple instances](http://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#spring-cloud-eureka-server-zones-and-regions).

### Eureka Server
Eureka server can update `defaultZone` and `hostname` to list all Eureka service URLs as follows:
```
-Deureka.client.serviceUrl.defaultZone=http://peer1:9761/eureka/,http://peer1:9761/eureka/
-Deureka.instance.hostname=${peer}
```

- `${peer}` - This represents the instance where eureka is running. It will be either `peer1` or `peer2` in this example

### Eureka Clients
Eureka clients can update the `defaultZone` to list all Eureka service URLs as follows:
```
-Deureka.client.serviceUrl.defaultZone=http://esb-msrvc-sit01.ei.local:9761/eureka/,http://esb-msrvc-sit02.ei.local:9761/eureka/ 
```

### Example for Sit environment
#### sit01 eureka server
```
-Deureka.client.serviceUrl.defaultZone=http://esb-msrvc-sit01.ei.local:9761/eureka/,http://esb-msrvc-sit02.ei.local:9761/eureka/  
-Deureka.instance.hostname=esb-msrvc-sit01.ei.local
```

#### sit01 Eureka clients
```
-Deureka.client.serviceUrl.defaultZone=http://esb-msrvc-sit01.ei.local:9761/eureka/,http://esb-msrvc-sit02.ei.local:9761/eureka/ 
```


#### sit02 Eureka server
```
-Deureka.client.serviceUrl.defaultZone=http://esb-msrvc-sit01.ei.local:9761/eureka/,http://esb-msrvc-sit02.ei.local:9761/eureka/  
-Deureka.instance.hostname=esb-msrvc-sit02.ei.local
```

#### sit02 Eureka clients
```
-Deureka.client.serviceUrl.defaultZone=http://esb-msrvc-sit01.ei.local:9761/eureka/,http://esb-msrvc-sit02.ei.local:9761/eureka/ 
```

## Good to know
- The Eureka web interface can be accessed from `http://localhost:9761`
- Log files can be located in `${logging.root}/eureka`
- Online documentation is available at `http://localhost:9761/doc`