package com.example.airlineservice.model;

import com.example.planeservice.model.Plane;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AirlineDto {
    private int id;
    private List<Plane> planes;
}
