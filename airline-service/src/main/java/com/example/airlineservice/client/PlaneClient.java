package com.example.airlineservice.client;

import com.example.planeservice.model.Plane;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "plane-service")
public interface PlaneClient {
    @GetMapping("/airline/{airlineId}")
    List<Plane> getPlanesByAirline(@PathVariable("airlineId") Integer airlineId);
}
