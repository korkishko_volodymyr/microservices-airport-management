package com.example.airlineservice.service;

import com.example.airlineservice.model.Airline;

import java.util.List;

public interface AirlineService {

    List<Airline> findByAirportId(Integer id);

    List<Airline> getAll();

    Airline findById(Integer id);

    void save(final Airline airline);

    void delete(Integer id);
}
