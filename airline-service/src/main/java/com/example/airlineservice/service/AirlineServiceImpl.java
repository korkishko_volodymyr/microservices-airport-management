package com.example.airlineservice.service;

import com.example.airlineservice.model.Airline;
import com.example.airlineservice.repo.AirlineRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirlineServiceImpl implements AirlineService {

    private final AirlineRepository airlineRepository;

    public AirlineServiceImpl(AirlineRepository airlineRepository) { this.airlineRepository = airlineRepository; }

    public void save(final Airline airline) { airlineRepository.save(airline); }

    public List<Airline> getAll() { return (List<Airline>) airlineRepository.findAll(); }

    public Airline findById(Integer id) { return airlineRepository.findById(id).orElse(new Airline()); }

    public void delete(Integer id){ airlineRepository.deleteById(id); }

    public List<Airline> findByAirportId(Integer id){ return airlineRepository.findByAirportId(id); }
}