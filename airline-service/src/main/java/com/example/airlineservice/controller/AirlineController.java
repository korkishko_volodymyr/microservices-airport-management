package com.example.airlineservice.controller;

import com.example.airlineservice.client.PlaneClient;
import com.example.airlineservice.model.Airline;
import com.example.airlineservice.model.AirlineDto;
import com.example.airlineservice.service.AirlineService;
import com.example.planeservice.model.Plane;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AirlineController {

    private final PlaneClient planeClient;
    private final AirlineService airlineService;

    public AirlineController(PlaneClient planeClient, AirlineService airlineService) {
        this.planeClient = planeClient;
        this.airlineService = airlineService;
    }

    @GetMapping
    public List<Airline> getAll(){ return airlineService.getAll(); }

    @GetMapping("/{airlineId}")
    public Airline get(@PathVariable("airlineId") Integer airlineId){ return airlineService.findById(airlineId); }

    @GetMapping("/{airlineId}/with-planes")
    public AirlineDto getPlanesByAirline(@PathVariable("airlineId") Integer airlineId){
        final List<Plane> planes = planeClient.getPlanesByAirline(airlineId);
        return new AirlineDto(airlineId,planes);
    }

    @GetMapping("/airport/{airportId}")
    public List<Airline> getAirlinesByAirport(@PathVariable("airportId") Integer airportId){
        return airlineService.findByAirportId(airportId);
    }

    @PostMapping
    public void save(@RequestBody Airline airline){ airlineService.save(airline); }

    @DeleteMapping("/{airlineId}")
    public void delete(@PathVariable("airlineId") Integer airlineId){ airlineService.delete(airlineId); }
}
