package com.example.airlineservice.repo;

import com.example.airlineservice.model.Airline;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirlineRepository extends CrudRepository<Airline, Integer> {
    List<Airline> findByAirportId(Integer airportId);
}
