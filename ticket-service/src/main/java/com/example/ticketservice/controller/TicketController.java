package com.example.ticketservice.controller;

import com.example.ticketservice.model.Ticket;
import com.example.ticketservice.service.TicketServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TicketController {

    private final TicketServiceImpl ticketService;

    public TicketController(TicketServiceImpl ticketService) { this.ticketService = ticketService; }

    @GetMapping
    public List<Ticket> getAll(){ return ticketService.getAll(); }

    @GetMapping("/{ticketId}")
    public Ticket get(@PathVariable("ticketId") Integer ticketId){ return ticketService.findById(ticketId); }

    @GetMapping("/passenger/{passengerId}")
    public List<Ticket> getTicketsByPassenger(@PathVariable("passengerId") Integer passengerId){
        return ticketService.findByPassenger(passengerId);
    }

    @GetMapping("/flight/{flightId}")
    public List<Ticket> getTicketsByFlight(@PathVariable("flightId") Integer flightId){
        return ticketService.findByFlight(flightId);
    }

    @PostMapping
    public void save(@RequestBody Ticket ticket){ ticketService.save(ticket); }

    @DeleteMapping("/{ticketId}")
    public void delete(@PathVariable("ticketId") Integer ticketId){ ticketService.delete(ticketId); }
}
