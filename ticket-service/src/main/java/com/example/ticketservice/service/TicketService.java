package com.example.ticketservice.service;

import com.example.ticketservice.model.Ticket;

import java.util.List;

public interface TicketService {

    List<Ticket> findByPassenger(int passengerId);

    List<Ticket> findByFlight(int flightId);

    List<Ticket> getAll();

    Ticket findById(Integer id);

    void save(final Ticket airline);

    void delete(Integer id);
}
