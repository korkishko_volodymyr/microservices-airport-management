package com.example.ticketservice.service;

import com.example.ticketservice.model.Ticket;
import com.example.ticketservice.repo.TicketRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    public TicketServiceImpl(TicketRepository ticketRepository) { this.ticketRepository = ticketRepository; }

    public void save(final Ticket ticket) {
        ticketRepository.save(ticket);
    }

    public List<Ticket> getAll() {
        return (List<Ticket>) ticketRepository.findAll();
    }

    public void delete(Integer id) { ticketRepository.deleteById(id); }

    public List<Ticket> findByPassenger(int passengerId) {return ticketRepository.findByPassengerId(passengerId);}

    public List<Ticket> findByFlight(int flightId) {return ticketRepository.findByFlightId(flightId);}

    public Ticket findById(Integer id){ return ticketRepository.findById(id).orElse(new Ticket());}

}