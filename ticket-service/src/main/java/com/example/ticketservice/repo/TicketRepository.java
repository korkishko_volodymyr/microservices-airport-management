package com.example.ticketservice.repo;

import com.example.ticketservice.model.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Integer> {
    List<Ticket> findByPassengerId(Integer passengerId);
    List<Ticket> findByFlightId(Integer flightId);
}
