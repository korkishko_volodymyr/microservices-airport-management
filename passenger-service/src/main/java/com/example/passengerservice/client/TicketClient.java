package com.example.passengerservice.client;

import com.example.ticketservice.model.Ticket;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "ticket-service")
public interface TicketClient {
    @GetMapping("/passenger/{passengerId}")
    List<Ticket> getTicketsByPassenger(@PathVariable("passengerId") Integer passengerId);
}
