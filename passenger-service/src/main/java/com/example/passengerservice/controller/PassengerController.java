package com.example.passengerservice.controller;

import com.example.passengerservice.client.TicketClient;
import com.example.passengerservice.model.Passenger;
import com.example.passengerservice.model.PassengerDto;
import com.example.passengerservice.service.PassengerService;
import com.example.passengerservice.service.PassengerServiceImpl;
import com.example.ticketservice.model.Ticket;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PassengerController {

    private final PassengerService passengerService;

    private final TicketClient ticketClient;

    public PassengerController(PassengerServiceImpl passengerService, TicketClient ticketClient) {
        this.passengerService = passengerService;
        this.ticketClient = ticketClient;
    }

    @GetMapping
    public List<Passenger> getAll(){
        return passengerService.getAll();
    }

    @GetMapping("/{passengerId}")
    public Passenger get(@PathVariable("passengerId") Integer passengerId){
        return passengerService.findById(passengerId);
    }

    @PostMapping
    public void save(@RequestBody Passenger passenger){
        passengerService.save(passenger);
    }

    @GetMapping("/{passengerId}/with-tickets")
    public PassengerDto getPassengerWithTickets(@PathVariable("passengerId") Integer passengerId){
        final List<Ticket> ticketsByPassenger = ticketClient.getTicketsByPassenger(passengerId);
        return new PassengerDto(passengerId,ticketsByPassenger);
    }

    @DeleteMapping("/{passengerId}")
    public void delete(@PathVariable("passengerId") Integer passengerId){ passengerService.delete(passengerId); }
}
