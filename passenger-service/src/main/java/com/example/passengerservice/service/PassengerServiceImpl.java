package com.example.passengerservice.service;

import com.example.passengerservice.model.Passenger;
import com.example.passengerservice.repo.PassengerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {

    private final PassengerRepository passengerRepository;

    public PassengerServiceImpl(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    public void save(final Passenger passenger) {
        passengerRepository.save(passenger);
    }

    public List<Passenger> getAll() {
        return (List<Passenger>) passengerRepository.findAll();
    }

    public void delete(Integer id) { passengerRepository.deleteById(id); }

    public Passenger findById(Integer id) { return passengerRepository.findById(id).orElse(new Passenger()); }
}
