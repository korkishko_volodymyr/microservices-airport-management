package com.example.passengerservice.service;

import com.example.passengerservice.model.Passenger;

import java.util.List;

public interface PassengerService {

    List<Passenger> getAll();

    Passenger findById(Integer id);

    void save(final Passenger airline);

    void delete(Integer id);
}
