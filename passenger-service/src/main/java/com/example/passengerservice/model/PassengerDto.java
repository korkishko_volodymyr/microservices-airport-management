package com.example.passengerservice.model;

import com.example.ticketservice.model.Ticket;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PassengerDto {
    private int id;
    private List<Ticket> ticketList;
}
