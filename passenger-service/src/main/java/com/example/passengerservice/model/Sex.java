package com.example.passengerservice.model;

public enum Sex {
    MALE, FEMALE
}
